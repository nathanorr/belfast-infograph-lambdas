package putWelcomeData;

public class UpdateWelcomeTableResponse {

    String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UpdateWelcomeTableResponse(String message) {
        this.message = message;
    }

    public UpdateWelcomeTableResponse() {
    }
}
