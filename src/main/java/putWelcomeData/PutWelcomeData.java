package putWelcomeData;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.Context;

public class PutWelcomeData implements RequestHandler<UpdateWelcomeTableRequest, UpdateWelcomeTableResponse> {

    private String TABLE_NAME = "WelcomeScreen";
    private String TABLE_KEY = "visitorDataID";
    private String TABLE_ATTRIBUTE_1 = "GreeterName";
    private String TABLE_ATTRIBUTE_2 = "VisitorName";
    private String TABLE_ATTRIBUTE_3 = "ContactNumber";

    public UpdateWelcomeTableResponse handleRequest(UpdateWelcomeTableRequest updateWelcomeTableRequest, Context context) {

        String visitorName = updateWelcomeTableRequest.visitorName;
        String greeterName = updateWelcomeTableRequest.greeterName;
        String meetingTime = updateWelcomeTableRequest.meetingTime;
        String contactNumber = updateWelcomeTableRequest.contactNumber;

        AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
                .build();
        DynamoDB dynamoDB = new DynamoDB(client);
        Table table = dynamoDB.getTable(TABLE_NAME);
        try {
            System.out.println("Adding a new item...");
            PutItemOutcome outcome = table
                    .putItem(new Item().withPrimaryKey(TABLE_KEY, meetingTime)
                            .withString(TABLE_ATTRIBUTE_1, greeterName)
                            .withString(TABLE_ATTRIBUTE_2, visitorName)
                            .withString(TABLE_ATTRIBUTE_3, contactNumber));
            System.out.println("PutItem succeeded:\n" + outcome.getPutItemResult());
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return new UpdateWelcomeTableResponse("Unable to add values to table  " + TABLE_NAME);
        }

        return new UpdateWelcomeTableResponse("Data successfully added to database");
    }

}
