package putWelcomeData;

public class UpdateWelcomeTableRequest {

    String visitorName;
    String greeterName;
    String meetingTime;
    String contactNumber;

    public String getMeetingTime() {
        return meetingTime;
    }

    public void setMeetingTime(String meetingTime) {
        this.meetingTime = meetingTime;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getVisitorName() {
        return visitorName;
    }

    public void setVisitorName(String visitorName) {
        this.visitorName = visitorName;
    }

    public String getGreeterName() {
        return greeterName;
    }

    public void setGreeterName(String greeterName) {
        this.greeterName = greeterName;
    }

    public UpdateWelcomeTableRequest(String visitorName, String greeterName) {
        this.visitorName = visitorName;
        this.greeterName = greeterName;
    }

    public UpdateWelcomeTableRequest() {
    }
}
