package getWelcomeData;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class GetWelcomeData implements RequestHandler<WelcomeDataRequest, WelcomeDataResponse> {

    private String TABLE_NAME = "WelcomeScreen";
    private String TABLE_KEY = "visitorDataID";
    private String TABLE_ATTRIBUTE_1 = "VisitorName";
    private String TABLE_ATTRIBUTE_2 = "GreeterName";
    private String TABLE_ATTRIBUTE_3 = "ContactNumber";

    public WelcomeDataResponse handleRequest(WelcomeDataRequest welcomeDataRequest, Context context) {

        AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
                .build();
        DynamoDB dynamoDB = new DynamoDB(client);
        Table table = dynamoDB.getTable(TABLE_NAME);

        WelcomeDataResponse welcomeDataResponse = new WelcomeDataResponse();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDateTime now = LocalDateTime.now();

        try {
            String tableExpression = TABLE_KEY + "," + TABLE_ATTRIBUTE_1 + ", " + TABLE_ATTRIBUTE_2 + ", " + TABLE_ATTRIBUTE_3 ;
            Item item = table.getItem(TABLE_KEY, dtf.format(now), tableExpression, null);

            System.out.println("Printing item after retrieving it....");
            System.out.println(item.toJSONPretty());

            welcomeDataResponse.setMeetingTime(item.get(TABLE_KEY).toString());
            welcomeDataResponse.setVisitorName(item.get(TABLE_ATTRIBUTE_1).toString());
            welcomeDataResponse.setGreeterName(item.get(TABLE_ATTRIBUTE_2).toString());
            welcomeDataResponse.setContactNumber(item.get(TABLE_ATTRIBUTE_3).toString());

        } catch (Exception e) {
            System.err.println("GetItem failed.");
            System.err.println(e.getMessage());
            return new WelcomeDataResponse(e.getMessage());
        }

        return welcomeDataResponse;
    }

}
