package getWelcomeData;

public class WelcomeDataResponse {

    private String visitorName;
    private String greeterName;
    private String meetingTime;
    private String contactNumber;
    private String errorMessage;

    public void setVisitorName(String visitorName) {
        this.visitorName = visitorName;
    }

    public void setGreeterName(String greeterName) {
        this.greeterName = greeterName;
    }

    public void setMeetingTime(String meetingTime) {
        this.meetingTime = meetingTime;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public WelcomeDataResponse() {
    }

    public WelcomeDataResponse(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
